<?php
class User_model extends CI_Model
{
    public $table = 'user';
    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    public function get_list(){
        $this->db->select('id,username,password,fullname');
        // $where = array('username'=>'thanhnha');
        // $this->db->where($where);
        // $this->db->limit(1,0);
        $query = $this->db->get('user');
        return $query->result();
    }
    public function get_user_byId($id){
        $this->db->select('id,username,password,fullname');
        $where = array('id'=>$id);
        $this->db->where($where);
        // $this->db->limit(1,0);
        $query = $this->db->get('user');
        return $query->result();
    }

    public function create($data){
        
        $this->db->insert('user',$data);
    }

    public function update($data,$id){
       
      
        $this->db->where(array('id'=>$id));
        $this->db->update('user',$data);


    }

    public function delete($id){
        $this->db->where(array('id'=>$id));
        $this->db->delete('user');
    }
}

?>