<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>thanhnha</title>
</head>
<body>
    <table border="1">
        <thead>
            <tr>User</tr>
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Password</th>
                <th>Full-name</th>
                <th>update</th>
                <th>delete</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $key => $value) { ?>
                    <tr>
                        <td><?php echo $value->id?></td>
                        <td><?php echo $value->username?></td>
                        <td><?php echo $value->password?></td>
                        <td><?php echo $value->fullname?></td>
                        <td><a href="<?php echo "http://localhost/codeigniter/home/update/".$value->id ?>">update</a></td>
                        <td><a href="<?php echo "http://localhost/codeigniter/home/delete/".$value->id ?>">delete</a></td>
                    </tr>
            <?}?>
        </tbody>
    </table>
    <form action="" method="post">
        <label for="username">username:</label>
        <input type="text" name="username" id="username" value="<?php echo set_value('username')?>">
        <div class="error" id="username_error"><?php echo form_error('username')?></div>

        <label for="username">password:</label>
        <input type="password" name="password" id="password">
        <div class="error" id="password_error"><?php echo form_error('password')?></div>

        <label for="username">r-pass:</label>
        <input type="password" name="re-password" id="re-password">
        <div class="error" id="re-password_error"><?php echo form_error('re-password')?></div>

        <label for="username">fullname:</label>
        <input type="text" name="fullname" id="fullname" value="<?php echo set_value('fullname')?>">
        <div class="error" id="fullname_error"><?php echo form_error('fullname')?></div>

        <input type="submit" name="submit" value="register" id="register">
    </form>
</body>
</html>