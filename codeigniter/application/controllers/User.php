<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
    class User extends CI_Controller
    {
        public function __construct(){
            parent::__construct();
            $this->load->model('user_model');
        }
        // phuong thuc dang ky thanh vien
        public function register(){
            //load library validation
            $this->load->library('form_validation');
            $this->load->helper('form');
      
            // $user = $this->user_model->get_list();
            //creatr rule
            $this->form_validation->set_rules('username','Username','require|xss_clean');
            $this->form_validation->set_rules('password','Password','require|min_lenght[8]|xss_clean');
            $this->form_validation->set_rules('re-password','Re-password','require|min_lenght[8]|matches[password]|xss_clean');
            $this->form_validation->set_rules('fullname','Username','require|xss_clean');

            if ($this->form_validation->run()) {
                $data = array(
                    'username'     => $this->input->post('username'),
                    'password' => ($this->input->post('password')),
                    'fullname'    => $this->input->post('fullname'),    
                 );
                $this->user_model->create($data);
            }
            // $data["list"]= $this->user_model->get_list();
            $this->load->view('index_view',$this->data);
        }
    }
    
    
        

?>