<?php
    class home extends CI_Controller
    {
        public function __construct(){
            parent::__construct();
            $this->load->model('user_model');
            $this->load->helper('url'); 
            $this->load->library('form_validation');
            $this->load->helper('form');
        }
        public function index(){
            redirect('home/register','refresh');
        }

        public function user(){
           
            
            $user = $this->user_model->get_list();
            echo '<pre>';
            print_r($user);
            // $data['user']=$user;
            // $this->load->view('index_view',$data);
        }

        

        public function update($id){
           
            var_dump($id);
           
            $this->form_validation->set_rules('username','Username','required');
            $this->form_validation->set_rules('password','Password','required|min_length[8]');
            $this->form_validation->set_rules('re-password','Re-password','required|min_length[8]|matches[password]');
            $this->form_validation->set_rules('fullname','fullname','required');

            if ($this->form_validation->run()) {
                if( $this->input->post('submit')!=null){
                $data = array(
                    'id' => $id ,
                    'username' => $this->input->post('username'),
                    'password' => $this->input->post('password'),
                    'fullname' => $this->input->post('fullname'),    
                 );
                    $this->user_model->update($data,$id);
                    redirect('home/register','refresh');
                }
            }
            else{
            $user = $this->user_model->get_user_byId($id);
            $data['user']=$user;
            $this->load->view('update',$data);
        }
        }
        public function delete($id){
            
            // if($this->input->get('id')!=null){
            //     $id = $this->input->get('id');
            //     $this->user_model->delete($id);
            // }
            var_dump($id);
            $this->user_model->delete($id);
            redirect('home/register','refresh');
        }
        public function register(){
            
            // $user = $this->user_model->get_list();
            //creatr rule
            $this->form_validation->set_rules('username','Username','required');
            $this->form_validation->set_rules('password','Password','required|min_length[8]');
            $this->form_validation->set_rules('re-password','Re-password','required|min_length[8]|matches[password]');
            $this->form_validation->set_rules('fullname','fullname','required');
            if ($this->form_validation->run()) {
                if( $this->input->post('submit')!=null){
                $data = array(
                    'username' => $this->input->post('username'),
                    'password' => $this->input->post('password'),
                    'fullname' => $this->input->post('fullname'),    
                 );
                $this->user_model->create($data);
                }
            }
            $list = $this->user_model->get_list();
            $data['list']= $list;
            $this->load->view('index_view',$data);
        }
    }
    
?>